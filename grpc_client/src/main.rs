pub mod database_exporter {
    tonic::include_proto!("database_exporter");
}

use database_exporter::database_exporter_client::DatabaseExporterClient;
use database_exporter::Row;
use futures_util::stream;
use rusqlite::{Connection, Result};
use std::env;
use tonic::transport::{Certificate, Channel, ClientTlsConfig};
use tonic::Request;

#[derive(Debug)]
struct DatabaseExporterService;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let server_host = env::var("GRPC_SERVER")?;
    let server_port = env::var("GRPC_PORT")?;
    let sqlite_conn = env::var("SQLITE_CONN")?;

    let cert_path = env::var("CERT_PATH")?;
    let key_path = env::var("KEY_PATH")?;

    let pem = tokio::fs::read(cert_path).await?;
    let key = tokio::fs::read(key_path).await?;
    let ca = Certificate::from_pem(pem);

    let tls = ClientTlsConfig::new()
        .ca_certificate(ca)
        .domain_name("grpc_server");

    let channel = Channel::from_shared(format!("http://{}:{}", server_host, server_port))?
        .tls_config(tls)?
        .connect()
        .await?;

    // let mut client =
    //     DatabaseExporterClient::connect(format!("http://{}:{}", server_host, server_port)).await?;
    let mut client = DatabaseExporterClient::new(channel);
    let conn = Connection::open(sqlite_conn)?;
    let mut stmt = conn.prepare("SELECT * FROM os")?;

    let mut v_rows = vec![];
    let mut q_rows = stmt.query([])?;

    while let Some(row) = q_rows.next()? {
        v_rows.push(Row {
            name: row.get(0)?,
            version: row.get(1)?,
            developer: row.get(2)?,
            country: row.get(3)?,
            release_date: row.get(4)?,
            license: row.get(5)?,
            kernel_name: row.get(6)?,
            kernel_type: row.get(7)?,
        })
    }

    let request = Request::new(stream::iter(v_rows));

    match client.load_entries(request).await {
        Ok(response) => println!("OK! {:?}", response.into_inner()),
        Err(e) => println!("Not OK :( {:?}", e),
    }

    Ok(())
}
