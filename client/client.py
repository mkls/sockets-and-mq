"""
Client that can send data to server
using sockets and message queues.
"""
import ssl
import socket
import os
import pika
import json
import time
import sqlite3
import uuid


class Client:
    def __init__(self, db_conn: str):
        self.db_conn = db_conn
        self.sent_count = 0
        self.corr_id = str(uuid.uuid4())
        self.response = False

    def send_socket(self, host: str, port: int, cert: str, key: str) -> None:
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)

        context.load_verify_locations(cert, key)

        print('Connecting to DB')
        db = sqlite3.connect(self.db_conn)
        db.row_factory = sqlite3.Row
        print('Connected!')
        cursor = db.cursor()
        data = cursor.execute('SELECT * FROM os')

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            conn = context.wrap_socket(s, server_hostname=host)
            conn.settimeout(10)

            with conn:
                # s.connect((host, port))
                conn.connect((host, port))

                print('Sending data...', flush=True)

                conn.sendall('['.encode('utf-8'))
                for item in data:
                    prep_item = json.dumps(dict(item)) + ','
                    print('Sending', json.dumps(dict(item)))
                    conn.sendall((prep_item).encode('utf-8'))
                conn.sendall(r']\q'.encode('utf-8'))

                resp = b""

                while not (r := conn.recv(1024)).endswith(rb'\q'):
                    resp += r
                resp += r

                if resp != rb'OK\q':
                    print('Server error')
                print('Received response', resp[:-2].decode())

        db.close()

    def on_mq_response(self, ch, method, props, body):
        print('Entered callback')

        if self.corr_id == props.correlation_id:
            self.response = True
            print(body)
            if body != "Ok!":
                self.sent_count = float('inf')
            self.sent_count += 1

    def send_mq(self, host: str, port: int) -> None:
        params = pika.ConnectionParameters(host=host, port=port)

        tries = 0
        success = False

        while tries < 10 and not success:
            tries += 1
            try:
                connection = pika.BlockingConnection(params)
                success = True
            except pika.exceptions.AMQPConnectionError as e:
                print(e)
                print('Failed to connect. Reconnecting in 5 seconds')
                time.sleep(5)

        if not success:
            print('Failed to connect. Shutting down')
            os.exit(1)

        print('Connected!')

        channel = connection.channel()
        res = channel.queue_declare(queue='', exclusive=True)

        print('Connecting to DB')
        db = sqlite3.connect(self.db_conn)
        db.row_factory = sqlite3.Row
        print('Connected!')
        cursor = db.cursor()

        callback_queue = res.method.queue
        print(callback_queue)
        channel.basic_consume(queue=callback_queue,
                              on_message_callback=self.on_mq_response,
                              auto_ack=True)

        cursor.execute('SELECT COUNT(*) FROM OS')
        count = cursor.fetchone()[0]

        data = cursor.execute('SELECT * FROM os')

        for item in data:
            channel.basic_publish(
                exchange='',
                routing_key='test',
                body=json.dumps(dict(item)).encode('utf-8'),
                properties=pika.BasicProperties(
                    # delivery_mode=2,
                    reply_to=callback_queue,
                    correlation_id=self.corr_id))

            while self.sent_count < count:
                connection.process_data_events()
            self.response = False

        db.close()
        connection.close()
