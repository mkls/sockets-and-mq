import os
from flask import Flask
from client import Client


def main():
    mq_host = os.getenv('MQ_HOST', None)
    mq_port = os.getenv('MQ_PORT', None)

    socket_host = os.getenv('SOCKET_HOST', None)
    socket_port = os.getenv('SOCKET_PORT', None)

    db_conn = os.getenv('SQLITE_CONN', None)

    cert = os.getenv('CERT_PATH', None)
    key = os.getenv('KEY_PATH', None)

    if not cert or not key:
        raise ValueError('CERT_PATH and KEYPATH envs must be supplied')

    if not mq_host or not mq_port or not socket_host or not socket_port:
        raise ValueError(
            'MQ_HOST, MQ_PORT, SOCKET_HOST, SOCKET_PORT must be supplied')
    if not db_conn:
        raise ValueError('SQLITE_CONN must be provided')

    client = Client(db_conn)

    app = Flask(__name__)

    @app.post('/mq')
    def send_to_mq():
        client.send_mq(mq_host, int(mq_port))
        return 'Request sent\n', 200

    @app.post('/socket')
    def send_to_socket():
        print('Sending to', socket_host, 'at', socket_port)
        client.send_socket(socket_host, int(socket_port), cert, key)
        return 'Data sent\n', 200

    app.run(host='0.0.0.0', port=5000)


if __name__ == '__main__':
    main()
