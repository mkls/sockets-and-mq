import os
from sys import argv
from socket_server import SocketServer
from mq_server import MqServer


def main():
    print('Starting the server')
    db_conn = os.getenv('DB_CONN', None)
    if not db_conn:
        raise ValueError('DB_CONN must be supplied')

    if len(argv) >= 2 and argv[1] == 'sockets':
        port = os.getenv('SOCKET_PORT', None)

        if not port:
            raise ValueError('SOCKET_PORT must be supplied')

        cert = os.getenv('CERT_PATH', None)
        key = os.getenv('KEY_PATH', None)

        if not cert or not key:
            raise ValueError('CERT_PATH and KEYPATH envs must be supplied')

        server = SocketServer("0.0.0.0", int(port), cert, key, db_conn)
    else:
        host = os.getenv('MQ_HOST', None)
        port = os.getenv('MQ_PORT', None)

        if not host or not port:
            raise ValueError('MQ_HOST and MQ_PORT must be supplied')

        server = MqServer(host, int(port), db_conn)

    server.start()


if __name__ == '__main__':
    main()
