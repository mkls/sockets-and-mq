"""
Simple worker-server that
gets messages through RabbitMQ
"""

import pika
import sys
import os
import time
import json
from db_exporter import DbExporter


class MqServer:
    def __init__(self, host: str, port: int, db_conn: str):
        """
        Construct new instance

        :param: host - RabbitMQ host
        :param: port - RabbitMQ host port
        """
        self.host = host
        self.port = port
        self.db_exporter = DbExporter(db_conn)

    def start(self):
        """
        Start server loop
        """
        params = pika.ConnectionParameters(host=self.host, port=self.port)

        tries = 0
        success = False

        while tries < 10 and not success:
            tries += 1

            try:
                connection = pika.BlockingConnection(params)
                success = True
            except pika.exceptions.AMQPConnectionError as e:
                print(e)
                print('Failed to connect. Reconnecting in 5 seconds')
                time.sleep(5)

        if not success:
            print('Failed to connect. Shutting down')
            os._exit(1)

        print('Connected!')

        channel = connection.channel()
        channel.queue_declare(queue='test')

        channel.basic_consume(queue='test',
                              on_message_callback=self.on_get_msg)

        print('Waiting for messages')

        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            print('Interrupted')
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)

    def on_get_msg(self, ch, method, props, body: bytes):
        """
        RabbitMQ message handler
        """
        # print(type(ch), type(method), type(props), type(body))
        # print(f'Received {body}')
        try:
            self.db_exporter.export_data([json.loads(body.decode('utf-8'))])
            response = "Ok!"
        except Exception as e:
            response = f"Something went wrong :( Msg: {e}"

        print(response, flush=True)
        print(props.reply_to)
        print(props.correlation_id)

        ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(
                             correlation_id=props.correlation_id),
                         body=response)
        ch.basic_ack(delivery_tag=method.delivery_tag)
