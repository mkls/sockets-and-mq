"""
TCP Socket server with TLS encryption
"""

import json
import socket
import ssl

from datetime import datetime
from db_exporter import DbExporter


class SocketServer:
    def __init__(self, host: str, port: int, cert: str, key: str,
                 db_conn: str):
        self.host = host
        self.port = port
        self.cert = cert
        self.key = key
        self.db_exporter = DbExporter(conn=db_conn)

    def start(self):
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        context.load_cert_chain(self.cert, self.key)

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()

            while True:
                conn, addr = s.accept()
                connstream = context.wrap_socket(conn, server_side=True)
                data = b''

                print('Connected by', addr)

                while not (d := connstream.recv(1024)).endswith(rb'\q'):
                    data += d
                data += d

                data = data.decode('utf-8')
                data = data[:-2]
                data = data[:-2] + data[-1:]

                connstream.sendall(b'OK\\q')

                # print(data, flush=True)
                print('Sent response', flush=True)

                data = list(json.loads(data))
                self.db_exporter.export_data(data)

                connstream.close()
            s.close()
