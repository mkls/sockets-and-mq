"""
Helper class to export rows
to Postgres database
"""
import psycopg2
from datetime import datetime
from psycopg2.sql import SQL, Identifier


class DbExporter:
    """
    Class that provides some methods
    to export data to Postgres database
    """
    def __init__(self, conn: str):
        """
        :param: conn - connection string
        """
        self.conn = conn

    def insert_row(self,
                   table: str,
                   row: dict,
                   comp_target=None,
                   ret_col='id',
                   allow_exist=True) -> int:
        """
        Inserts new row to a database

        :param: table - table to insert new row
        :param: row - row to insert
        :param: allow_exist - if not true, fail on existing data
        :returns: None
        """
        conn = psycopg2.connect(self.conn)
        cur = conn.cursor()

        insert_query = SQL('''
            INSERT INTO {table} ({cols}) VALUES ({values})
            ON CONFLICT DO NOTHING RETURNING {ret_col}
        ''').format(table=Identifier(row['table']),
                    cols=SQL(',').join(map(Identifier, row['columns'])),
                    values=SQL(('%s,' * len(row['values']))[:-1]),
                    ret_col=Identifier(ret_col))

        if comp_target is not None:
            select_query = SQL('''
                SELECT id FROM {}
                WHERE {} = %s
            ''').format(Identifier(table), Identifier(comp_target))

            cur.execute(select_query, (row[comp_target], ))
            count = cur.fetchone()
        else:
            count = None

        if count is None:
            cur.execute(insert_query, row['values'])
            res = cur.fetchone()

            if res is not None:
                res = res[0]
        else:
            res = count[0]

        conn.commit()
        conn.close()

        return res

    def export_data(self, data: list):
        for row in data:
            # Kernel type
            if row['kernel_type'] is not None:
                d = {
                    'columns': ('name', ),
                    'name': row['kernel_type'],
                    'values': (row['kernel_type'], ),
                    'table': 'kernel_type'
                }
                kernel_type_id = self.insert_row('kernel_type', d, 'name')
            else:
                kernel_type_id = None

            # Kernel
            if row['kernel_name'] is not None:
                d = {
                    'columns': ('name', 'type_id'),
                    'name': row['kernel_name'],
                    'values': (row['kernel_name'], kernel_type_id),
                    'table': 'kernel'
                }
                kernel_id = self.insert_row('kernel', d, 'name')
            else:
                kernel_id = None

            # License
            if row['license'] is not None:
                d = {
                    'columns': ('name', ),
                    'name': row['license'],
                    'values': (row['license'], ),
                    'table': 'license'
                }
                license_id = self.insert_row('license', d, 'name')
            else:
                license_id = None

            # Country
            if row['developer_country'] is not None:
                d = {
                    'columns': ('name', ),
                    'name': row['developer_country'],
                    'values': (row['developer_country'], ),
                    'table': 'country'
                }
                country_id = self.insert_row('country', d, 'name')
            else:
                country_id = None

            # Developer
            if row['developer'] is not None:
                d = {
                    'columns': ('name', 'country_id'),
                    'name': row['developer'],
                    'values': (row['developer'], country_id),
                    'table': 'developer'
                }
                dev_id = self.insert_row('developer', d, 'name')
            else:
                dev_id = None

            # OS
            d = {
                'columns': ('name', ),
                'name': row['name'],
                'values': (row['name'], ),
                'table': 'os'
            }
            os_id = self.insert_row('os', d, 'name')

            # OS version
            d = {
                'columns': ('os_id', 'version', 'dev_id', 'release_date',
                            'license_id', 'kernel_id'),
                'values': (os_id, row['version'], dev_id,
                           datetime.fromisoformat(row['release_date']),
                           license_id, kernel_id),
                'table':
                'os_version'
            }
            self.insert_row('os_version', d, None, ret_col='os_id')
