import service_pb2
import service_pb2_grpc
from db_exporter import DbExporter


class GrpcServer(service_pb2_grpc.DatabaseExporterServicer):
    def __init__(self, db_conn: str):
        self.db_exporter = DbExporter(db_conn)

    def LoadEntries(self, request_iterator, context):
        for row in request_iterator:
            d = {
                'name': row.name,
                'version': row.version,
                'developer': row.developer,
                'developer_country': row.country,
                'release_date': row.release_date,
                'license': row.license,
                'kernel_name': row.kernel_name,
                'kernel_type': row.kernel_type
            }
            self.db_exporter.export_data([d])
        return service_pb2.Status(msg="OK")
