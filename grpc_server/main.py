import grpc
import service_pb2_grpc
import logging
import os
from server import GrpcServer
from concurrent import futures


def main():
    db_conn = os.getenv('DB_CONN')

    key = os.getenv('KEY_PATH')
    cert = os.getenv('CERT_PATH')

    with open(key, 'rb') as f:
        key = f.read()

    with open(cert, 'rb') as f:
        cert = f.read()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=4))
    service_pb2_grpc.add_DatabaseExporterServicer_to_server(
        GrpcServer(db_conn), server)
    server_creds = grpc.ssl_server_credentials(((key, cert), ))
    # server.add_insecure_port('[::1]:50052')
    server.add_secure_port('0.0.0.0:50051', server_creds)
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level='DEBUG')
    main()
